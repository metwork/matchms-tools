import re
from pathlib import Path
from io import StringIO
import numpy as np
from matchms import Spikes
from matchms.importing import load_from_mgf as matchms_load_from_mgf
from matchms.filtering import default_filters, normalize_intensities, reduce_to_number_of_peaks
from matchms.similarity import ModifiedCosine
from .utils import merge_spectrums
from .spectrum import Spectrum
from .plotter import SpectrumPlotter


class SpectrumSet(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._cosine = ModifiedCosine()

    @classmethod
    def load_from_mgf(cls, path):
        mgf = (
            Spectrum.from_spectrum(spectrum) for spectrum in matchms_load_from_mgf(path)
        )
        spectrums = cls()
        for spectrum in mgf:
            metadata = spectrum.metadata
            metadata["precursor_mz"] = metadata["pepmass"][0]
            spectrum.metadata = metadata
            spectrum = cls.prepare_spectrum(spectrum)
            spectrums[spectrum.metadata["scans"]] = Spectrum.from_spectrum(spectrum)
        return spectrums

    @classmethod
    def load_from_cfm_id(cls, path):
        raw_text = Path(path).read_text()
        data = re.split(r"(energy\d)\n", raw_text)[1:]
        spectrums = cls()
        for energy, spectrum in pairwise(data):
            spectrum = np.loadtxt(StringIO(spectrum))
            spectrum = Spectrum(mz=spectrum[:, 0], intensities=spectrum[:, 1])
            spectrum = cls.prepare_spectrum(spectrum)
            spectrums[energy] = spectrum
        return spectrums

    @staticmethod
    def prepare_spectrum(spectrum):
        # Apply default filter to standardize ion mode, correct charge and more.
        # Default filter is fully explained at https://matchms.readthedocs.io/en/latest/api/matchms.filtering.html .
        mz, intensities = spectrum.peaks
        intensities = np.sqrt(intensities) * 10
        spectrum.peaks = Spikes(mz=mz, intensities=intensities)
        # spectrum = reduce_to_number_of_peaks(spectrum, n_max=50)
        spectrum = default_filters(spectrum)
        # Scale peak intensities to maximum of 1
        spectrum = normalize_intensities(spectrum)
        return Spectrum.from_spectrum(spectrum)

    def _get_from_ids(self, spectrum_ids):
        spectrums = self.__class__()
        for idx in spectrum_ids:
            spectrums[idx] = self[idx]
        return spectrums

    def merge(self, spectrum_ids=None):
        if spectrum_ids is None:
            spectrum_ids = self.keys()
        spectrums = self._get_from_ids(spectrum_ids)
        return merge_spectrums(spectrums)

    def plot(self, id_1, id_2=None, **kwargs):
        plotter = SpectrumPlotter()
        if id_2 is None:
            return self[id_1].plot(**kwargs)
        else:
            spectrums = self._get_from_ids((id_1, id_2))
            return plotter.plot_double(spectrums, feature_ids=(id_1, id_2), **kwargs)

    def cosine(self, *pair_idx):
        if pair_idx is not None and len(pair_idx) == 2:
            pair = [self[idx] for idx in pair_idx]
            return self._cosine.pair(*pair)
        else:
            values = list(self.values())
            return self._cosine.matrix(
                references=values, queries=values, is_symmetric=True
            )


def pairwise(iterable):
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    a = iter(iterable)
    return zip(a, a)
