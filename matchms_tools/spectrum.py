from matchms import Spectrum as MatchmsSpectrum
from matchms_tools.plotter import SpectrumPlotter


class Spectrum(MatchmsSpectrum):
    @classmethod
    def from_spectrum(cls, spectrum):
        return cls(
            mz=spectrum.peaks.mz,
            intensities=spectrum.peaks.intensities,
            metadata=spectrum.metadata,
        )

    def plot(self):
        plotter = SpectrumPlotter()
        return plotter.plot(self)
