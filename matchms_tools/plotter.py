import pandas as pd
import numpy as np
import plotly.graph_objects as go


class SpectrumPlotter:
    figure = None

    def __init__(self, annot_threshold=1):
        self.annot_threshold = annot_threshold

    def plot(self, spectrum, annot_threshold=1, threshold=0, **kwargs):

        self.set_figure(**kwargs)
        self.add_trace(self.get_spectrum_data(spectrum))
        self.figure.update_traces(line=dict(width=1))
        return self.figure

    def plot_double(self, spectrums, feature_ids=None, **kwargs):

        self.set_figure(**kwargs)
        if isinstance(spectrums, dict):
            spectrums = spectrums.values()
        if feature_ids:
            iterator = zip(feature_ids, spectrums)
        else:
            iterator = enumerate(spectrums)
        factor = 1
        for name, spectrum in iterator:
            data = self.get_spectrum_data(spectrum)
            data["y"] = [self.factor_value(value, factor) for value in data["y"]]
            self.add_trace(data, name=name)
            factor *= -1
        self.figure.update_traces(line=dict(width=1))
        return self.figure

    def factor_value(self, value, factor):
        if value is not None:
            value = value * factor
        return value

    def set_figure(self, **kwargs):
        fig = go.Figure()
        fig.update_layout(
            title=kwargs.get("title"),
            xaxis_title="m/z",
            yaxis_title="Normalized intensity",
        )
        self.figure = fig

    def add_trace(self, data, name=""):
        self.figure.add_trace(
            go.Scatter(
                x=data["x"],
                y=data["y"],
                mode="lines+markers",
                name=name,
            )
        )

    def get_spectrum_data(self, spectrum, annot_threshold=0):
        df = pd.DataFrame(
            {"intensities": spectrum.peaks.intensities, "mz": spectrum.peaks.mz}
        )
        # df = df[df.intensity > threshold].reset_index()
        df["blank"] = None
        df["zero"] = 0
        df["mz1"] = df.mz
        df["mz2"] = df.mz
        return {
            "x": np.concatenate(
                df.loc[:, [f"mz{idx}" for idx in ("", "1", "2")]].values
            ),
            "y": np.concatenate(df.loc[:, ("zero", "intensities", "blank")].values),
        }
